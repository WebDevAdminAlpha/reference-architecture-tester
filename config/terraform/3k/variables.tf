variable "project" {
  description = "GCP Project"
}

variable "region" {
  description = "GCP Region"
}

variable "zone" {
  description = "GCP Zone"
}

variable "prefix" {
  description = "Instance prefix"
}
