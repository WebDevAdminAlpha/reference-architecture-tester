module "gitlab_ref_arch_gcp" {
  source = "../modules/gitlab_ref_arch_gcp"

  prefix = var.prefix
  project = var.project
  machine_image = "ubuntu-2004-lts"

  elastic_node_count = 1
  elastic_machine_type = "n1-standard-4"

  gitlab_nfs_node_count = 1
  gitlab_nfs_machine_type = "n1-highcpu-4"

  gitlab_rails_node_count = 1
  gitlab_rails_machine_type = "n1-highcpu-8"

  haproxy_external_node_count = 1
  haproxy_external_machine_type = "n1-highcpu-2"

  monitor_node_count = 1
  monitor_machine_type = "n1-highcpu-2"
}

output "gitlab_ref_arch_gcp" {
  value = module.gitlab_ref_arch_gcp
}
