module "gitlab_ref_arch_gcp" {
  source = "../modules/gitlab_ref_arch_gcp"

  prefix = var.prefix
  project = var.project
  machine_image = "ubuntu-2004-lts"

  consul_node_count = 3
  consul_machine_type = "n1-standard-1"

  postgres_node_count = 3
  postgres_machine_type = "n1-standard-1"

  gitlab_rails_node_count = 1
  gitlab_rails_machine_type = "n1-standard-1"

  pgbouncer_node_count = 1
  pgbouncer_machine_type = "n1-standard-1"

  # GET must-haves
  gitlab_nfs_node_count = 1
  gitlab_nfs_machine_type = "n1-standard-1"
  haproxy_external_node_count = 1
  haproxy_external_machine_type = "n1-standard-1"
  haproxy_internal_node_count = 1
  haproxy_internal_machine_type = "n1-standard-1"
}

output "gitlab_ref_arch_gcp" {
  value = module.gitlab_ref_arch_gcp
}
