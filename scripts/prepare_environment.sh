#!/bin/bash

shopt -s extglob

if [ $# -eq 0 ]; then
  echo "Usage: prepare_environment <terraform|ansible>"
  exit 1
fi

if ! [ -d secrets ]; then mkdir secrets; fi

if ! [ -e "${GCP_SERVICE_ACCOUNT_FILE}" ]; then
  echo "${GCP_SERVICE_ACCOUNT_JSON}" > "${GCP_SERVICE_ACCOUNT_FILE}"
fi

if [ "$1" == "terraform" ]; then
  cp -vr config/terraform/${REFERENCE_ARCHITECTURE}/* gitlab-environment-toolkit/terraform/environments
elif [ "$1" == "ansible" ]; then
  if ! [ -e "${SSH_PRIVATE_KEY_FILE}" ]; then
    echo "${SSH_PRIVATE_KEY}" > "${SSH_PRIVATE_KEY_FILE}" && chmod 0600 "${SSH_PRIVATE_KEY_FILE}"
  fi

  if ! [ -e "${GITLAB_LICENSE_FILE}" ]; then
    echo "${GITLAB_LICENSE}" > "${GITLAB_LICENSE_FILE}"
  fi

  cp -vr config/ansible gitlab-environment-toolkit/ansible/inventories/rat

  sed -i "s|<RAT_GCP_PREFIX>|${TF_VAR_prefix}|g;
          s|<RAT_GCP_PROJECT>|${TF_VAR_project}|g;
          s|<RAT_ANSIBLE_USER>|${ANSIBLE_USER}|g;
          s|<RAT_EXTERNAL_IP>|${EXTERNAL_IP}|g;
          s|<RAT_GITLAB_INITIAL_PASSWORD>|${GITLAB_INITIAL_PASSWORD}|g;
          s|<RAT_SSH_PRIVATE_KEY_FILE>|${SSH_PRIVATE_KEY_FILE}|g;
          s|<RAT_GITLAB_LICENSE_FILE>|${GITLAB_LICENSE_FILE}|g;
          s|<RAT_GCP_SERVICE_ACCOUNT_HOST_FILE>|${GCP_SERVICE_ACCOUNT_FILE}|g;" gitlab-environment-toolkit/ansible/inventories/rat/*.yml

  if [ -n "${PACKAGE_URL}" ]; then
    echo "Downloading package from ${PACKAGE_URL}"
    if [[ ${PACKAGE_URL} == https://gitlab.com/api/v4* ]]; then
      if [ -z "${GITLAB_API_TOKEN}" ]; then
        echo "Downloading from GitLab API requires GITLAB_API_TOKEN to be defined. Exiting."
        exit 1
      fi

      rm -rf /tmp/gitlab.deb
      wget --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" "${PACKAGE_URL}" -O /tmp/gitlab.deb

      sed -i "s|# gitlab_deb_host_path|gitlab_deb_host_path|g" gitlab-environment-toolkit/ansible/inventories/rat/vars.yml
    else
      wget "${PACKAGE_URL}" -O /tmp/gitlab.deb
    fi
  else
    if [ "$NIGHTLY" == "true" ]; then
      sed -i "s|# gitlab_repo_script_url.*|gitlab_repo_script_url: \"https://packages.gitlab.com/install/repositories/gitlab/nightly-builds/script.deb.sh\"|g" gitlab-environment-toolkit/ansible/inventories/rat/vars.yml
    fi

    if [ -n "${PACKAGE_VERSION}" ]; then
        sed -i "s|# gitlab_repo_package.*|gitlab_repo_package: \"${PACKAGE_VERSION}\"|g" gitlab-environment-toolkit/ansible/inventories/rat/vars.yml
    fi
  fi
fi
